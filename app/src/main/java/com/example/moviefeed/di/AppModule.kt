package com.example.moviefeed.di

import android.app.Application
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Context
import com.example.moviefeed.common.viewmodel.ViewModelFactory
import com.example.moviefeed.data.local.MoviesDatabase
import com.example.moviefeed.data.local.dao.*
import com.example.moviefeed.data.remote.ApiConfig.API_BASE_URL
import com.example.moviefeed.data.remote.ApiConfig.API_KEY
import com.example.moviefeed.data.remote.ApiConfig.API_KEY_QUERY
import com.example.moviefeed.data.remote.MoviesApiService
import com.example.moviefeed.data.remote.QueryParamInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Dagger [Module] responsible of make Application-wide components injectable
 *
 * @author santiagoalvarez
 */
@Module(subcomponents = [(ViewModelSubComponent::class)])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application.applicationContext

    @Singleton
    @Provides
    fun provideMoviesApiService(): MoviesApiService {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val interceptor = QueryParamInterceptor(API_KEY_QUERY, API_KEY)
        if (!okHttpClientBuilder.interceptors().contains(interceptor)) {
            okHttpClientBuilder.addInterceptor(interceptor)
        }
        return Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
                .create(MoviesApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideMoviesDatabase(context: Context): MoviesDatabase {
        return Room.databaseBuilder(context.applicationContext,
                MoviesDatabase::class.java, "Movies.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideConfigurationDao(db: MoviesDatabase): ConfigurationDao = db.configurationDao()

    @Singleton
    @Provides
    fun provideGenreDao(db: MoviesDatabase): GenreDao = db.genreDao()

    @Singleton
    @Provides
    fun provideMoviesDao(db: MoviesDatabase): MoviesDao = db.moviesDao()

    @Singleton
    @Provides
    fun provideTvShowDao(db: MoviesDatabase): TvShowDao = db.tvShowDao()

    @Singleton
    @Provides
    fun provideMovieDetailDao(db: MoviesDatabase): MovieDetailDao = db.movieDetailDao()

    @Singleton
    @Provides
    fun provideTvShowDetailDao(db: MoviesDatabase): TvShowDetailDao = db.tvShowDetailDao()

    @Singleton
    @Provides
    fun provideViewModelFactory(viewModelSubComponent: ViewModelSubComponent.Builder): ViewModelProvider.Factory =
            ViewModelFactory(viewModelSubComponent.build())
}