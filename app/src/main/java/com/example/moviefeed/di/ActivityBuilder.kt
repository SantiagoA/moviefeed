package com.example.moviefeed.di

import com.example.moviefeed.detail.DetailActivity
import com.example.moviefeed.detail.DetailModule
import com.example.moviefeed.home.HomeActivity
import com.example.moviefeed.home.HomeModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Dagger [Module] responsible of create subcomponents of activities that includes the specified modules for parent [AppComponent]
 *
 * @author santiagoalvarez
 */
@Module
abstract class ActivityBuilder {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [HomeModule::class])
    internal abstract fun bindHomeActivity(): HomeActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [DetailModule::class])
    internal abstract fun bindDetailActivity(): DetailActivity
}