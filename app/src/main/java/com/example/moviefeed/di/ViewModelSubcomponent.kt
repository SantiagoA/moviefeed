package com.example.moviefeed.di

import com.example.moviefeed.detail.DetailViewModel
import com.example.moviefeed.home.HomeViewModel
import dagger.Subcomponent

/**
 * Dagger [Subcomponent] responsible of create View Model instances
 *
 * @author santiagoalvarez
 */
@Subcomponent
interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): ViewModelSubComponent
    }

    fun homeViewModel(): HomeViewModel
    fun detailViewModel(): DetailViewModel
}