package com.example.moviefeed.detail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.example.moviefeed.data.MovieManagerImpl
import com.example.moviefeed.data.Resource
import com.example.moviefeed.data.Status
import com.example.moviefeed.detail.model.DetailItem
import com.example.moviefeed.domain.FeedType
import com.example.moviefeed.util.movieDetailToDetailItem
import com.example.moviefeed.util.tvShowDetailToDetailItem
import javax.inject.Inject

/**
 * Class that represents a [ViewModel] for Detail section. Responsible of communication between Views and data layers
 *
 * @author santiagoalvarez
 */
class DetailViewModel @Inject constructor(private val manager: MovieManagerImpl) : ViewModel() {

    var currentUserSelection = MutableLiveData<Pair<FeedType, Int>>()
    var detailItem: LiveData<Resource<DetailItem>> = Transformations.switchMap(currentUserSelection, {
        loadData(it)
    })

    private fun loadData(viewPair: Pair<FeedType, Int>): LiveData<Resource<DetailItem>> =
            when (viewPair.first) {
                FeedType.MOVIE -> Transformations.map(manager.getMovieDetail(viewPair.second), {
                    when (it.status) {
                        Status.SUCCESS -> Resource.success(it.data?.movieDetailToDetailItem())
                        Status.LOADING -> Resource.loading(it.data?.movieDetailToDetailItem())
                        Status.ERROR -> Resource.error(it.message, it.data?.movieDetailToDetailItem())
                    }
                })
                FeedType.TV_SHOW -> Transformations.map(manager.getTvShowDetail(viewPair.second), {
                    when (it.status) {
                        Status.SUCCESS -> Resource.success(it.data?.tvShowDetailToDetailItem())
                        Status.LOADING -> Resource.loading(it.data?.tvShowDetailToDetailItem())
                        Status.ERROR -> Resource.error(it.message, it.data?.tvShowDetailToDetailItem())
                    }
                })
            }
}