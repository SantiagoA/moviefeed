package com.example.moviefeed.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import com.example.moviefeed.R
import com.example.moviefeed.common.ImageLoaderListener
import com.example.moviefeed.common.base.BaseActivity
import com.example.moviefeed.data.Status
import com.example.moviefeed.databinding.ActivityDetailBinding
import com.example.moviefeed.detail.model.DetailItem
import com.example.moviefeed.domain.FeedType
import com.example.moviefeed.navigation.FragmentNavigationEntry
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject

/**
 * This [Activity] show a detail window
 *
 * @author santiagoalvarez
 */
class DetailActivity : BaseActivity() {

    private var itemId: Int = 0
    private lateinit var currentFeedType: FeedType
    private lateinit var detailBinding: ActivityDetailBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: DetailViewModel

    companion object {
        const val EXTRA_ID = "id"
        const val EXTRA_FEED_TYPE = "feed_type"

        fun createIntent(context: Context, id: Int, feedType: FeedType) =
                Intent(context, DetailActivity::class.java).apply {
                    putExtra(EXTRA_ID, id)
                    putExtra(EXTRA_FEED_TYPE, feedType)
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        initVars()
        initViewModel()
    }

    private fun initVars() {
        itemId = intent.getIntExtra(EXTRA_ID, 0)
        currentFeedType = intent.getSerializableExtra(EXTRA_FEED_TYPE) as FeedType
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
        viewModel.detailItem.observe(this, Observer {
            when (it?.status) {
                Status.SUCCESS -> {
                    supportPostponeEnterTransition()
                    val detailItem: DetailItem? = it.data
                    if (detailItem != null) {
                        detailBinding.item = detailItem
                        detailBinding.listener = object : ImageLoaderListener {
                            override fun onLoadFail(e: Throwable) {
                                supportStartPostponedEnterTransition()
                            }

                            override fun onLoadSuccess() {
                                supportStartPostponedEnterTransition()
                            }
                        }
                        FragmentNavigationEntry.Builder(navigator, DetailFragment.newInstance(detailItem.id))
                                .noPush()
                                .navigate()
                    }
                }
                Status.ERROR -> {
                    appbar.setExpanded(false)
                    FragmentNavigationEntry.Builder(navigator, DetailErrorFragment())
                            .noPush()
                            .navigate()
                }
                Status.LOADING -> {//no-op
                }
            }
        })
        viewModel.currentUserSelection.value = currentFeedType to itemId
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    supportFinishAfterTransition()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
