package com.example.moviefeed.detail

import com.example.moviefeed.di.FragmentScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Dagger [Module] for make [DetailFragment] injectable
 *
 * @author santiagoalvarez
 */
@Module
abstract class DetailModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun bindDetailFragment(): DetailFragment
}