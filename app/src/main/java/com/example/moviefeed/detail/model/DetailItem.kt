package com.example.moviefeed.detail.model

import com.example.moviefeed.data.local.model.Credits
import com.example.moviefeed.data.local.model.Genre
import com.example.moviefeed.data.local.model.ProductionCompany
import com.example.moviefeed.data.local.model.ProductionCountry

/**
 * Class that maps a [MovieDetail] or [TvShowDetail] to UI model for Detail section
 *
 * @author santiagoalvarez
 */
data class DetailItem(
        val adult: Boolean,
        val backdropPath: String,
        val budget: Int,
        val genres: List<Genre>,
        val homepage: String,
        val id: Int,
        val imdbId: String,
        val originalLanguage: String,
        val originalTitle: String,
        val overview: String,
        val popularity: Double,
        val posterPath: String,
        val productionCompanies: List<ProductionCompany>,
        val productionCountries: List<ProductionCountry>?,
        val releaseDate: String,
        val revenue: Int,
        val runtime: Int,
        val spokenLanguages: List<String>,
        val status: String,
        val tagline: String,
        val title: String,
        val video: Boolean,
        val voteAverage: Float,
        val voteCount: Int,
        val credits: Credits
)