package com.example.moviefeed.home

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.moviefeed.domain.FeedFilterType

/**
 * Pager Adapter for [HomeActivity]
 *
 * @author santiagoalvarez
 */
class HomePagerAdapter(fragmentManager: FragmentManager, private val titlesArray: Array<String>) :
        FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): HomeFragment = HomeFragment()

    override fun getCount(): Int = FeedFilterType.values().size

    override fun getPageTitle(position: Int): CharSequence? = titlesArray[position]
}