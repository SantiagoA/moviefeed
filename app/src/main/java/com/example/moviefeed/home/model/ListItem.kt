package com.example.moviefeed.home.model

import com.example.moviefeed.data.local.model.Movie
import com.example.moviefeed.data.local.model.TvShow
import com.example.moviefeed.domain.FeedType

/**
 * Class that maps a [Movie] or [TvShow] to UI model for Home section
 *
 * @author santiagoalvarez
 */
data class ListItem(
        val feedType: FeedType,
        val id: Int,
        val releaseDate: String,
        val posterPath: String,
        val overview: String,
        val genreIds: List<Int>,
        val originalTitle: String,
        val originalLanguage: String,
        val title: String,
        val backdropPath: String,
        val popularity: Double,
        val voteCount: Int,
        val voteAverage: Float
) {
    constructor(movie: Movie) : this(
            FeedType.MOVIE,
            movie.id,
            movie.releaseDate,
            movie.posterPath,
            movie.overview,
            movie.genreIds,
            movie.originalTitle,
            movie.originalLanguage,
            movie.title,
            movie.backdropPath,
            movie.popularity,
            movie.voteCount,
            movie.voteAverage.toFloat())

    constructor(tvShow: TvShow) : this(
            FeedType.TV_SHOW,
            tvShow.id,
            tvShow.firstAirDate,
            tvShow.posterPath,
            tvShow.overview,
            tvShow.genreIds,
            tvShow.originalName,
            tvShow.originalLanguage,
            tvShow.name,
            tvShow.backdropPath,
            tvShow.popularity,
            tvShow.voteCount,
            tvShow.voteAverage.toFloat())
}