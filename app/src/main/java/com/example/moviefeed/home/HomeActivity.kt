package com.example.moviefeed.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.SharedElementCallback
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import com.example.moviefeed.App
import com.example.moviefeed.R
import com.example.moviefeed.common.base.BaseActivity
import com.example.moviefeed.detail.DetailActivity
import com.example.moviefeed.domain.FeedFilterType
import com.example.moviefeed.domain.FeedType
import com.example.moviefeed.navigation.IntentNavigationEntry
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_home_list.*
import javax.inject.Inject

/**
 * This [Activity] show the Home window
 *
 * @author santiagoalvarez
 */
class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
        ViewPager.OnPageChangeListener, UserInteractionListener {

    private val CURRENT_FEED_TYPE_KEY = "current_feed_type"
    private val CURRENT_FILTER_TYPE_KEY = "current_filter_type"
    private val CURRENT_SELECTED_POSITION_KEY = "current_selected_position"

    private var currentFeedType: FeedType = FeedType.MOVIE
    private var currentFeedFilterType: FeedFilterType = FeedFilterType.POPULAR
    internal var currentSelectedPosition: Int = RecyclerView.NO_POSITION
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initDrawer()
        initPager()
        initViewModel()
        initSharedTransitions()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            with(it) {
                currentFeedType = FeedType.valueOf(getString(CURRENT_FEED_TYPE_KEY))
                currentFeedFilterType = FeedFilterType.valueOf(getString(CURRENT_FILTER_TYPE_KEY))
                currentSelectedPosition = getInt(CURRENT_SELECTED_POSITION_KEY)
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        val menuItem = when (currentFeedType) {
            FeedType.MOVIE -> nav_view.menu.findItem(R.id.nav_movies)
            FeedType.TV_SHOW -> nav_view.menu.findItem(R.id.nav_tv)
        }
        onNavigationItemSelected(menuItem)
        nav_view.setCheckedItem(menuItem.itemId)

        vPLanding.currentItem = currentFeedFilterType.ordinal
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.let {
            with(it) {
                putString(CURRENT_FEED_TYPE_KEY, currentFeedType.name)
                putString(CURRENT_FILTER_TYPE_KEY, currentFeedFilterType.name)
                putInt(CURRENT_SELECTED_POSITION_KEY, currentSelectedPosition)
            }
        }
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    //region Init
    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        viewModel.configuration.observe(this, Observer {
            if (it?.data != null) {
                App.instance.configuration = it.data
            }
        })
    }

    private fun initDrawer() {
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun initPager() {
        with(vPLanding) {
            adapter = HomePagerAdapter(supportFragmentManager, resources.getStringArray(R.array.filter_titles))
            addOnPageChangeListener(this@HomeActivity)
            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tLLanding))
        }
        with(tLLanding) {
            setupWithViewPager(vPLanding)
            addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(vPLanding))
        }
    }

    private fun initSharedTransitions() {
        supportPostponeEnterTransition()
        setExitSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(names: MutableList<String>?, sharedElements: MutableMap<String, View>?) {
                super.onMapSharedElements(names, sharedElements)
                val viewHolder = rCList.findViewHolderForAdapterPosition(currentSelectedPosition)
                if (viewHolder != null && names != null && !names.isEmpty() && sharedElements != null) {
                    sharedElements[names[0]] = viewHolder.itemView.findViewById(R.id.itemImage)
                }
            }
        })
    }
    //endregion

    //region Listeners
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_movies -> {
                supportActionBar?.title = item.title
                currentFeedType = FeedType.MOVIE
            }

            R.id.nav_tv -> {
                supportActionBar?.title = item.title
                currentFeedType = FeedType.TV_SHOW
            }
        }

        setCurrentViewPair()
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        currentFeedFilterType = FeedFilterType.values()[position]
        onRefresh()// force refresh
    }

    override fun onRefresh() {
        viewModel.refreshData()
        setCurrentViewPair()
    }

    override fun onListItemClick(view: View, position: Int, id: Int) {
        currentSelectedPosition = position
        IntentNavigationEntry.Builder(navigator, DetailActivity.createIntent(this, id, currentFeedType))
                .withOptions(ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, id.toString()).toBundle())
                .navigate()
    }
    //endregion

    private fun setCurrentViewPair() {
        viewModel.currentUserSelection.value = currentFeedType to currentFeedFilterType
    }
}
