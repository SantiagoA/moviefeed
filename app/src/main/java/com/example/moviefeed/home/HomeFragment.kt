package com.example.moviefeed.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.moviefeed.R
import com.example.moviefeed.databinding.FragmentHomeListBinding
import com.example.moviefeed.di.ActivityScoped
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home_list.*
import javax.inject.Inject

/**
 * Home [Fragment] in charge of display [ViewPager] page
 *
 * @author santiagoalvarez
 */
@ActivityScoped
class HomeFragment @Inject constructor() : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: HomeViewModel
    private lateinit var dataBinding: FragmentHomeListBinding
    private lateinit var userInteractionListener: UserInteractionListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        check(context is UserInteractionListener, { "Activity must implement UserInteractionListener" })
        userInteractionListener = context as UserInteractionListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //activity is the owner of this live data
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(HomeViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.resources.observe(this, Observer {
            dataBinding.resource = it
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_home_list, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(dataBinding) {
            userInteractionListener = this@HomeFragment.userInteractionListener
        }
        with(rCList) {
            adapter = HomeRecyclerViewAdapter(mutableListOf(), userInteractionListener)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun onResume() {
        super.onResume()
        val selectedPosition = (activity as HomeActivity).currentSelectedPosition
        if (selectedPosition != RecyclerView.NO_POSITION) {
            rCList.scrollToPosition(selectedPosition)
            ((activity as HomeActivity).supportStartPostponedEnterTransition())
        }
    }
}
