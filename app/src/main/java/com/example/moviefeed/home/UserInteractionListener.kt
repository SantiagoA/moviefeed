package com.example.moviefeed.home

import android.view.View

/**
 * Callback that notifies user interactions on Home section
 *
 * @author santiagoalvarez
 */
interface UserInteractionListener {

    fun onRefresh()

    fun onListItemClick(view: View, position: Int, id: Int)
}