package com.example.moviefeed.home

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.example.moviefeed.data.MovieManagerImpl
import com.example.moviefeed.data.Resource
import com.example.moviefeed.data.Status
import com.example.moviefeed.data.local.model.Configuration
import com.example.moviefeed.data.local.model.Movie
import com.example.moviefeed.data.local.model.TvShow
import com.example.moviefeed.domain.FeedFilterType
import com.example.moviefeed.domain.FeedType
import com.example.moviefeed.home.model.ListItem
import com.example.moviefeed.util.moviesToListItems
import com.example.moviefeed.util.tvShowsToListItems
import javax.inject.Inject

/**
 * @author santiagoalvarez
 */

class HomeViewModel @Inject constructor(private val manager: MovieManagerImpl) : ViewModel() {

    var configuration: LiveData<Resource<Configuration>> = manager.getConfiguration()
    var currentUserSelection = MutableLiveData<Pair<FeedType, FeedFilterType>>()
    val resources: LiveData<Resource<List<ListItem>>> = Transformations.switchMap(currentUserSelection, {
        loadData(it)
    })

    fun refreshData() {
        manager.refreshData()
    }

    private fun loadData(viewPair: Pair<FeedType, FeedFilterType>): LiveData<Resource<List<ListItem>>>? =
            when (viewPair.first) {
                FeedType.MOVIE -> Transformations.map(loadMovies(viewPair.second), {
                    when (it.status) {
                        Status.SUCCESS -> Resource.success(it.data?.moviesToListItems())
                        Status.LOADING -> Resource.loading(it.data?.moviesToListItems())
                        Status.ERROR -> Resource.error(it.message, it.data?.moviesToListItems())
                    }
                })
                FeedType.TV_SHOW -> Transformations.map(loadTvShows(viewPair.second), {
                    when (it.status) {
                        Status.SUCCESS -> Resource.success(it.data?.tvShowsToListItems())
                        Status.LOADING -> Resource.loading(it.data?.tvShowsToListItems())
                        Status.ERROR -> Resource.error(it.message, it.data?.tvShowsToListItems())
                    }
                })
            }

    private fun loadMovies(filterType: FeedFilterType): LiveData<Resource<List<Movie>>> =
            when (filterType) {
                FeedFilterType.POPULAR -> loadPopularMovies()
                FeedFilterType.TOP_RATED -> loadTopRatedMovies()
                FeedFilterType.UPCOMING -> loadUpcomingMovies()
            }

    private fun loadTvShows(filterType: FeedFilterType): LiveData<Resource<List<TvShow>>> =
            when (filterType) {
                FeedFilterType.POPULAR -> loadPopularTvShows()
                FeedFilterType.TOP_RATED -> loadTopRatedTvShows()
                FeedFilterType.UPCOMING -> loadUpcomingTvShows()
            }

    private fun loadPopularMovies() = manager.getPopularMovies()

    private fun loadTopRatedMovies() = manager.getTopRatedMovies()

    private fun loadUpcomingMovies() = manager.getUpcomingMovies()

    private fun loadPopularTvShows() = manager.getPopularTvShows()

    private fun loadTopRatedTvShows() = manager.getTopRatedTvShows()

    private fun loadUpcomingTvShows() = manager.getUpcomingTvShows()
}