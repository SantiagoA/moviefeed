package com.example.moviefeed.util

import com.example.moviefeed.data.local.model.*
import com.example.moviefeed.data.remote.model.*
import com.example.moviefeed.detail.model.DetailItem
import com.example.moviefeed.domain.Cast
import com.example.moviefeed.domain.Crew
import com.example.moviefeed.home.model.ListItem

/**
 * Kotlin File that holds mappers to transform data between layers
 *
 * @author santiagoalvarez
 */

fun Movie.toListItem() = ListItem(this)

fun TvShow.toListItem() = ListItem(this)

fun List<Movie>.moviesToListItems() = map { it.toListItem() }

fun List<TvShow>.tvShowsToListItems() = map { it.toListItem() }

fun ApiImages.toImage() = Image(baseUrl, secureBaseUrl, backdropSizes, logoSizes, posterSizes, profileSizes, stillSizes)

fun ApiConfigurationResponse.toConfiguration() = Configuration(apiImages.toImage(), changeKeys)

fun ApiMovie.toMovie() = Movie(id, overview, posterPath, backdropPath, genreIds, popularity, voteCount, voteAverage, adult, releaseDate, originalTitle, originalLanguage, title, video)

fun ApiTvShow.toTvShow() = TvShow(id, overview, posterPath, backdropPath, genreIds, popularity, voteCount, voteAverage, firstAirDate, originCountry, originalLanguage, name, originalName)

fun List<ApiMovie>.apiMoviesToMovies() = map { it.toMovie() }

fun List<ApiTvShow>.apiTvShowsToTvShows() = map { it.toTvShow() }

fun ApiMovieDetail.toMovieDetail() = MovieDetail(
        adult,
        backdropPath,
        budget,
        genres.toGenres(),
        homepage,
        id,
        imdbId,
        originalLanguage,
        originalTitle,
        overview,
        popularity,
        posterPath,
        apiProductionCompanies.toProductionCompanies(),
        apiProductionCountries.toProductionCountries(),
        releaseDate,
        revenue,
        runtime,
        apiSpokenLanguages.toSpokenLanguages(),
        status,
        tagline,
        title,
        video,
        voteAverage,
        voteCount,
        credits.toCredits())

fun ApiTvShowDetail.toTvShowDetail() = TvShowDetail(
        backdropPath,
        apiCreatedBy.toCreatedBy(),
        episodeRunTime,
        firstAirDate,
        genres.toGenres(),
        homepage,
        id,
        inProduction,
        languages,
        lastAirDate,
        name,
        networks.toNetworks(),
        numberOfEpisodes,
        numberOfSeasons,
        originCountry,
        originalLanguage,
        originalName,
        overview,
        popularity,
        posterPath,
        productionCompanies.toProductionCompanies(),
        apiSeasons.toSeasons(),
        status,
        type,
        voteAverage,
        voteCount,
        credits.toCredits())

fun ApiNameIdPair.toNetwork() = Network(id, name)

fun ApiNameIdPair.toGenre() = Genre(id, name)

fun ApiNameIdPair.toProductionCompany() = ProductionCompany(id, name)

fun List<ApiNameIdPair>.toGenres() = map { it.toGenre() }

fun List<ApiNameIdPair>.toNetworks() = map { it.toNetwork() }

fun List<ApiNameIdPair>.toProductionCompanies() = map { it.toProductionCompany() }

fun ApiSpokenLanguage.toSpokenLanguage() = SpokenLanguage(iso, name)

fun ApiProductionCountry.toProductionCountry() = ProductionCountry(iso, name)

fun List<ApiSpokenLanguage>.toSpokenLanguages() = map { it.toSpokenLanguage() }

fun List<ApiProductionCountry>.toProductionCountries() = map { it.toProductionCountry() }

fun ApiCreatedBy.toCreatedBy() = CreatedBy(id, name, gender, profilePath)

fun List<ApiCreatedBy>.toCreatedBy() = map { it.toCreatedBy() }

fun ApiSeason.toSeason() = Season(airDate, episodeCount, id, posterPath, seasonNumber)

fun List<ApiSeason>.toSeasons() = map { it.toSeason() }

fun ApiCredits.toCredits() = Credits(cast.toCasts(), crew.toCrews())

fun List<ApiCast>.toCasts() = map { it.toCast() }

fun ApiCast.toCast() = Cast(castId, character, creditId, gender, id, name, order, profilePath)

fun List<ApiCrew>.toCrews() = map { it.toCrew() }

fun ApiCrew.toCrew() = Crew(creditId, department, gender, id, job, name, profilePath)

fun MovieDetail.movieDetailToDetailItem() = DetailItem(
        adult,
        backdropPath,
        budget,
        genres,
        homepage,
        id,
        imdbId,
        originalLanguage,
        originalTitle,
        overview,
        popularity,
        posterPath,
        productionCompanies,
        productionCountries,
        releaseDate,
        revenue,
        runtime,
        spokenLanguages.map { it.name },
        status,
        tagline,
        title,
        video,
        voteAverage.toFloat(),
        voteCount,
        credits)

fun TvShowDetail.tvShowDetailToDetailItem() = DetailItem(
        false,
        backdropPath,
        0,
        genres,
        homepage,
        id,
        "",
        originalLanguage,
        originalName,
        overview,
        popularity,
        posterPath,
        productionCompanies,
        null,
        firstAirDate,
        0,
        0,
        languages,
        status,
        "",
        name,
        false,
        voteAverage.toFloat(),
        voteCount,
        credits)