package com.example.moviefeed.util

import java.util.*
import java.util.concurrent.TimeUnit

/**
 * [Date] extension for common operation
 *
 * @author santiagoalvarez
 */

fun Date.daysBetween(date: Date): Long {
    val diff = this.time - date.time
    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
}

fun Date.daysBetween(time: Long): Long {
    val diff = this.time - time
    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
}