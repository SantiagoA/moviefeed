package com.example.moviefeed.common.base

import android.os.Bundle
import android.util.Log
import com.example.moviefeed.R
import com.example.moviefeed.navigation.NavigationEntry
import com.example.moviefeed.navigation.Navigator
import com.example.moviefeed.navigation.Navigator.NavigationListener
import dagger.android.support.DaggerAppCompatActivity

/**
 * BaseActivity responsible of init [Navigator] and Dagger
 *
 * @author santiagoalvarez
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    companion object {
        const val TAG = "BaseActivity"
    }

    protected lateinit var navigator: Navigator
    lateinit var navigationListener: NavigationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigationListener = object : NavigationListener {
            override fun onNavigate(navigator: Navigator, entry: NavigationEntry<*>): Boolean {
                Log.d(TAG, "attempt to navigate to: " + entry.title)
                return false
            }

        }
        navigator = Navigator(this, savedInstanceState, R.id.container, navigationListener)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (outState != null) {
            navigator.saveInstanceState(outState)
        }
    }
}