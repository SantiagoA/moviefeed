package com.example.moviefeed

import com.example.moviefeed.data.local.model.Configuration
import com.example.moviefeed.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * @author santiagoalvarez
 */
class App : DaggerApplication() {

    var configuration: Configuration? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: App
            private set
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}