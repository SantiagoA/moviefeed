package com.example.moviefeed.domain

/**
 * @author santiagoalvarez
 */
enum class FeedFilterType {
    POPULAR, TOP_RATED, UPCOMING
}