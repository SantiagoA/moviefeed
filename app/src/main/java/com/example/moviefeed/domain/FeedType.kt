package com.example.moviefeed.domain

/**
 * @author santiagoalvarez
 */
enum class FeedType {
    MOVIE, TV_SHOW
}