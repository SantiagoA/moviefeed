package com.example.moviefeed.data.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Database model for TVShow
 *
 * @author santiagoalvarez
 */
@Entity(tableName = "tv_show")
data class TvShow(
        @PrimaryKey var id: Int,
        var overview: String,
        var posterPath: String,
        var backdropPath: String,
        var genreIds: List<Int>,
        var popularity: Double,
        var voteCount: Int,
        var voteAverage: Double,
        var firstAirDate: String,
        var originCountry: List<String>,
        var originalLanguage: String,
        var name: String,
        var originalName: String
)