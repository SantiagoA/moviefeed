package com.example.moviefeed.data

import android.arch.lifecycle.LiveData
import com.example.moviefeed.data.local.model.*

/**
 * This manager is the entry point to data layer, exposes all available operations over data for clients
 *
 * @author santiagoalvarez
 */
interface MovieManager {

    /**
     * Get the [Configuration] wrapped in [Resource] to exposes it's state, returned as [LiveData]
     */
    fun getConfiguration(): LiveData<Resource<Configuration>>

    /**
     * Get a [List] of Popular [Movie]'s as [LiveData]
     */
    fun getPopularMovies(): LiveData<Resource<List<Movie>>>

    /**
     * Get a [List] of Top rated [Movie]'s as [LiveData]
     */
    fun getTopRatedMovies(): LiveData<Resource<List<Movie>>>

    /**
     * Get a [List] of Upcoming [Movie]'s as [LiveData]
     */
    fun getUpcomingMovies(): LiveData<Resource<List<Movie>>>

    /**
     * Get a [List] of Popular [TvShow]'s as [LiveData]
     */
    fun getPopularTvShows(): LiveData<Resource<List<TvShow>>>

    /**
     * Get a [List] of Top rated [TvShow]'s as [LiveData]
     */
    fun getTopRatedTvShows(): LiveData<Resource<List<TvShow>>>

    /**
     * Get a [List] of Upcoming [TvShow]'s as [LiveData]
     */
    fun getUpcomingTvShows(): LiveData<Resource<List<TvShow>>>

    /**
     * Get the [MovieDetail] wrapped in [Resource] to exposes it's state, returned as [LiveData]
     * @param id of the wanted [MovieDetail]
     */
    fun getMovieDetail(id: Int): LiveData<Resource<MovieDetail>>

    /**
     * Get the [TvShowDetail] wrapped in [Resource] to exposes it's state, returned as [LiveData]
     * @param id of the wanted [TvShowDetail]
     */
    fun getTvShowDetail(id: Int): LiveData<Resource<TvShowDetail>>

    /**
     * Get a [List] of Movie's [Genre]'s as [LiveData]
     */
    fun getMovieGenresList(): LiveData<Resource<List<Genre>>>

    /**
     * Get a [List] of TvShow's [Genre]'s as [LiveData]
     */
    fun getTvShowGenresList(): LiveData<Resource<List<Genre>>>

    /**
     * Get a [List] of result of the performed query
     * @param query
     */
    fun getMultiSearch(query: String): LiveData<List<*>>
}