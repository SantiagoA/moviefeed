package com.example.moviefeed.data.local.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.moviefeed.data.local.model.TvShowDetail

/**
 * Dao for [TvShowDetail] entity
 *
 * @author santiagoalvarez
 */
@Dao
abstract class TvShowDetailDao : BaseDao<TvShowDetail> {

    @Query("SELECT * FROM tv_show_detail WHERE id = :id")
    abstract fun getTvShowDetail(id: Int): LiveData<TvShowDetail>

    @Query("DELETE FROM tv_show_detail")
    abstract fun deleteAll()

    @Transaction
    open fun updateMovieDetail(tvShowDetail: TvShowDetail) {
        deleteAll()
        insert(tvShowDetail)
    }
}