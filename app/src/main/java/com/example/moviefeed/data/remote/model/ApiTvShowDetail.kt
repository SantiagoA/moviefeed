package com.example.moviefeed.data.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author santiagoalvarez
 */
data class ApiTvShowDetail(
        @SerializedName("id") val id: Int,
        @SerializedName("overview") val overview: String,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("backdrop_path") val backdropPath: String,
        @SerializedName("genre_ids") val genreIds: List<Int>,
        @SerializedName("original_language") val originalLanguage: String,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("created_by") val apiCreatedBy: List<ApiCreatedBy>,
        @SerializedName("episode_run_time") val episodeRunTime: List<Int>,
        @SerializedName("first_air_date") val firstAirDate: String,
        @SerializedName("genres") val genres: List<ApiNameIdPair>,
        @SerializedName("homepage") val homepage: String,
        @SerializedName("in_production") val inProduction: Boolean,
        @SerializedName("languages") val languages: List<String>,
        @SerializedName("last_air_date") val lastAirDate: String,
        @SerializedName("name") val name: String,
        @SerializedName("networks") val networks: List<ApiNameIdPair>,
        @SerializedName("number_of_episodes") val numberOfEpisodes: Int,
        @SerializedName("number_of_seasons") val numberOfSeasons: Int,
        @SerializedName("origin_country") val originCountry: List<String>,
        @SerializedName("original_name") val originalName: String,
        @SerializedName("production_companies") val productionCompanies: List<ApiNameIdPair>,
        @SerializedName("seasons") val apiSeasons: List<ApiSeason>,
        @SerializedName("status") val status: String,
        @SerializedName("type") val type: String,
        @SerializedName("credits") val credits: ApiCredits
)

data class ApiSeason(
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episode_count") val episodeCount: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("season_number") val seasonNumber: Int
)

data class ApiCreatedBy(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("gender") val gender: Int,
        @SerializedName("profile_path") val profilePath: String
)