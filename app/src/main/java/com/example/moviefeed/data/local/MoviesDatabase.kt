package com.example.moviefeed.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.moviefeed.data.local.dao.*
import com.example.moviefeed.data.local.model.*
import com.example.moviefeed.util.Converters

/**
 * Class that represent a SQLite Database
 *
 * @author santiagoalvarez
 */
@Database(entities = [(Configuration::class), (Movie::class), (TvShow::class), (MovieDetail::class), (TvShowDetail::class), (Genre::class)], version = 2)
@TypeConverters(Converters::class)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun configurationDao(): ConfigurationDao

    abstract fun moviesDao(): MoviesDao

    abstract fun tvShowDao(): TvShowDao

    abstract fun movieDetailDao(): MovieDetailDao

    abstract fun tvShowDetailDao(): TvShowDetailDao

    abstract fun genreDao(): GenreDao

}