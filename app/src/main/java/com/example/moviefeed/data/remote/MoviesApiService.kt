package com.example.moviefeed.data.remote

import com.example.moviefeed.data.remote.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author santiagoalvarez
 */
interface MoviesApiService {

    @GET("configuration?api_key")
    fun getConfiguration(): Call<ApiConfigurationResponse>

    @GET("movie/popular?api_key&page=1")
    fun getPopularMovies(): Call<BaseApiResponse<ApiMovie>>

    @GET("movie/top_rated?api_key&page=1")
    fun getTopRatedMovies(): Call<BaseApiResponse<ApiMovie>>

    @GET("movie/upcoming?api_key&page=1")
    fun getUpcomingMovies(): Call<BaseApiResponse<ApiMovie>>

    @GET("tv/popular?api_key&page=1")
    fun getPopularTvShows(): Call<BaseApiResponse<ApiTvShow>>

    @GET("tv/top_rated?api_key&page=1")
    fun getTopRatedTvShows(): Call<BaseApiResponse<ApiTvShow>>

    @GET("tv/on_the_air?api_key&page=1")
    fun getUpcomingTvShows(): Call<BaseApiResponse<ApiTvShow>>

    @GET("movie/{movie_id}?api_key&append_to_response=credits")
    fun getMovieDetail(@Path("movie_id") id: Int): Call<ApiMovieDetail>

    @GET("tv/{tv_id}?api_key&append_to_response=credits")
    fun getTvShowDetail(@Path("tv_id") id: Int): Call<ApiTvShowDetail>

    @GET("/genre/movie/list?api_key")
    fun getMovieGenresList(): Call<ApiGenresResponse>

    @GET("/genre/tv/list?api_key")
    fun getTvShowGenresList(): Call<ApiGenresResponse>

    @GET("/search/multi?api_key")
    fun getMultiSearch(@Query("query") query: String): Call<List<*>>
}