package com.example.moviefeed.data

import android.arch.lifecycle.LiveData
import com.example.moviefeed.data.local.dao.*
import com.example.moviefeed.data.local.model.*
import com.example.moviefeed.data.remote.MoviesApiService
import com.example.moviefeed.data.remote.model.*
import com.example.moviefeed.domain.FeedFilterType
import com.example.moviefeed.util.*
import retrofit2.Call
import java.util.*
import javax.inject.Inject

/**
 * Implementation of the [MovieManager]
 *
 * @author santiagoalvarez
 */
class MovieManagerImpl @Inject constructor(
        private val moviesApiService: MoviesApiService,
        private val configurationDao: ConfigurationDao,
        private var moviesDao: MoviesDao,
        private val tvShowDao: TvShowDao,
        private val movieDetailDao: MovieDetailDao,
        private val tvShowDetailDao: TvShowDetailDao,
        private val genresDao: GenreDao) : MovieManager {

    private val MAX_CONFIG_AGE = 5
    private var dirty: Boolean = false

    fun refreshData() {
        dirty = true
    }

    override fun getConfiguration(): LiveData<Resource<Configuration>> {
        return object : NetworkBoundResource<Configuration, ApiConfigurationResponse>() {

            override fun saveCallResult(item: ApiConfigurationResponse) {
                configurationDao.updateConfiguration(item.toConfiguration())
            }

            override fun loadFromDb(): LiveData<Configuration> {
                return configurationDao.getConfiguration()
            }

            override fun createCall(): Call<ApiConfigurationResponse> {
                return moviesApiService.getConfiguration()
            }

            override fun shouldFetch(data: Configuration?): Boolean {
                return data == null || Date().daysBetween(data.dateAdded) > MAX_CONFIG_AGE
            }
        }.asLiveData
    }

    override fun getPopularMovies() = getMovies(FeedFilterType.POPULAR)

    override fun getTopRatedMovies() = getMovies(FeedFilterType.TOP_RATED)

    override fun getUpcomingMovies() = getMovies(FeedFilterType.UPCOMING)

    private fun getMovies(filterType: FeedFilterType): LiveData<Resource<List<Movie>>> {
        val response: LiveData<List<Movie>>
        val request: Call<BaseApiResponse<ApiMovie>>
        when (filterType) {
            FeedFilterType.POPULAR -> {
                request = moviesApiService.getPopularMovies()
                response = moviesDao.getPopularMovies()
            }
            FeedFilterType.TOP_RATED -> {
                request = moviesApiService.getTopRatedMovies()
                response = moviesDao.getTopRatedMovies()
            }
            FeedFilterType.UPCOMING -> {
                request = moviesApiService.getUpcomingMovies()
                response = moviesDao.getUpcomingMovies()
            }
        }
        return object : NetworkBoundResource<List<Movie>, BaseApiResponse<ApiMovie>>() {

            override fun saveCallResult(item: BaseApiResponse<ApiMovie>) {
                moviesDao.updateMovies(item.results.apiMoviesToMovies())
            }

            override fun loadFromDb(): LiveData<List<Movie>> {
                return response
            }

            override fun createCall(): Call<BaseApiResponse<ApiMovie>> {
                return request
            }

            override fun shouldFetch(data: List<Movie>?): Boolean {
                return dirty || data == null || data.isEmpty()
            }

        }.asLiveData
    }

    override fun getPopularTvShows() = getTvShows(FeedFilterType.POPULAR)

    override fun getTopRatedTvShows() = getTvShows(FeedFilterType.TOP_RATED)

    override fun getUpcomingTvShows() = getTvShows(FeedFilterType.UPCOMING)

    private fun getTvShows(filterType: FeedFilterType): LiveData<Resource<List<TvShow>>> {
        val response: LiveData<List<TvShow>>
        val request: Call<BaseApiResponse<ApiTvShow>>
        when (filterType) {
            FeedFilterType.POPULAR -> {
                request = moviesApiService.getPopularTvShows()
                response = tvShowDao.getPopulaTvShows()
            }
            FeedFilterType.TOP_RATED -> {
                request = moviesApiService.getTopRatedTvShows()
                response = tvShowDao.getTopRatedTvShows()
            }
            FeedFilterType.UPCOMING -> {
                request = moviesApiService.getUpcomingTvShows()
                response = tvShowDao.getUpcomingTvShows()
            }
        }
        return object : NetworkBoundResource<List<TvShow>, BaseApiResponse<ApiTvShow>>() {

            override fun saveCallResult(item: BaseApiResponse<ApiTvShow>) {
                tvShowDao.updateTvShows(item.results.apiTvShowsToTvShows())
            }

            override fun loadFromDb(): LiveData<List<TvShow>> {
                return response
            }

            override fun createCall(): Call<BaseApiResponse<ApiTvShow>> {
                return request
            }

            override fun shouldFetch(data: List<TvShow>?): Boolean {
                return dirty || data == null || data.isEmpty()
            }

        }.asLiveData
    }

    override fun getMovieDetail(id: Int): LiveData<Resource<MovieDetail>> {
        return object : NetworkBoundResource<MovieDetail, ApiMovieDetail>() {

            override fun saveCallResult(item: ApiMovieDetail) {
                movieDetailDao.insert(item.toMovieDetail())
            }

            override fun loadFromDb(): LiveData<MovieDetail> {
                return movieDetailDao.getMovieDetail(id)
            }

            override fun createCall(): Call<ApiMovieDetail> {
                return moviesApiService.getMovieDetail(id)
            }

            override fun shouldFetch(data: MovieDetail?): Boolean {
                return dirty || data == null
            }
        }.asLiveData
    }

    override fun getTvShowDetail(id: Int): LiveData<Resource<TvShowDetail>> {
        return object : NetworkBoundResource<TvShowDetail, ApiTvShowDetail>() {

            override fun saveCallResult(item: ApiTvShowDetail) {
                tvShowDetailDao.insert(item.toTvShowDetail())
            }

            override fun loadFromDb(): LiveData<TvShowDetail> {
                return tvShowDetailDao.getTvShowDetail(id)
            }

            override fun createCall(): Call<ApiTvShowDetail> {
                return moviesApiService.getTvShowDetail(id)
            }

            override fun shouldFetch(data: TvShowDetail?): Boolean {
                return dirty || data == null
            }
        }.asLiveData
    }

    override fun getMovieGenresList(): LiveData<Resource<List<Genre>>> {
        return object : NetworkBoundResource<List<Genre>, ApiGenresResponse>() {

            override fun saveCallResult(item: ApiGenresResponse) {
                genresDao.updateGenre(item.apiGenres.toGenres())
            }

            override fun loadFromDb(): LiveData<List<Genre>> {
                return genresDao.getGenres()
            }

            override fun createCall(): Call<ApiGenresResponse> {
                return moviesApiService.getMovieGenresList()
            }

            override fun shouldFetch(data: List<Genre>?): Boolean {
                return dirty || data == null
            }
        }.asLiveData
    }

    override fun getTvShowGenresList(): LiveData<Resource<List<Genre>>> {
        return object : NetworkBoundResource<List<Genre>, ApiGenresResponse>() {

            override fun saveCallResult(item: ApiGenresResponse) {
                genresDao.updateGenre(item.apiGenres.toGenres())
            }

            override fun loadFromDb(): LiveData<List<Genre>> {
                return genresDao.getGenres()
            }

            override fun createCall(): Call<ApiGenresResponse> {
                return moviesApiService.getTvShowGenresList()
            }

            override fun shouldFetch(data: List<Genre>?): Boolean {
                return dirty || data == null
            }
        }.asLiveData
    }

    override fun getMultiSearch(query: String): LiveData<List<*>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}