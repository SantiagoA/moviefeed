package com.example.moviefeed.data.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Database model for Genre
 *
 * @author santiagoalvarez
 */
@Entity(tableName = "genre")
data class Genre(@PrimaryKey val id: Int, val name: String)