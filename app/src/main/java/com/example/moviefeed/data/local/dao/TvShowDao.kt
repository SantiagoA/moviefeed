package com.example.moviefeed.data.local.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.moviefeed.data.local.model.TvShow

/**
 * Dao for [TvShow] entity
 *
 * @author santiagoalvarez
 */
@Dao
abstract class TvShowDao : BaseDao<TvShow> {

    @Query("SELECT * FROM tv_show")
    abstract fun getTvShows(): List<TvShow>

    @Query("SELECT * FROM tv_show ORDER BY popularity DESC")
    abstract fun getPopulaTvShows(): LiveData<List<TvShow>>

    @Query("SELECT * FROM tv_show ORDER BY voteAverage DESC")
    abstract fun getTopRatedTvShows(): LiveData<List<TvShow>>

    @Query("SELECT * FROM tv_show WHERE firstAirDate > date('now') ORDER BY firstAirDate DESC")
    abstract fun getUpcomingTvShows(): LiveData<List<TvShow>>

    @Query("SELECT * FROM tv_show WHERE id = :id")
    abstract fun getTvShow(id: Int): TvShow

    @Query("DELETE FROM tv_show")
    abstract fun deleteAll()

    @Transaction
    open fun updateTvShows(list: List<TvShow>) {
        deleteAll()
        insert(*list.toTypedArray())
    }
}