package com.example.moviefeed.data.remote.model

import com.google.gson.annotations.SerializedName


/**
 * @author santiagoalvarez
 */

data class ApiGenresResponse(@SerializedName("genres") val apiGenres: List<ApiNameIdPair>)