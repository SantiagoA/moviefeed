package com.example.moviefeed.data.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @author santiagoalvarez
 */
data class ApiTvShow(
        @SerializedName("id") val id: Int,
        @SerializedName("overview") val overview: String,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("backdrop_path") val backdropPath: String,
        @SerializedName("genre_ids") val genreIds: List<Int>,
        @SerializedName("original_language") val originalLanguage: String,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("first_air_date") val firstAirDate: String,
        @SerializedName("origin_country") val originCountry: List<String>,
        @SerializedName("name") val name: String,
        @SerializedName("original_name") val originalName: String
)