package com.example.moviefeed.data.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Database model for TvShow detail
 *
 * @author santiagoalvarez
 */
@Entity(tableName = "tv_show_detail")
data class TvShowDetail(
        var backdropPath: String,
        var createdBy: List<CreatedBy>,
        var episodeRunTime: List<Int>,
        var firstAirDate: String,
        var genres: List<Genre>,
        var homepage: String,
        @PrimaryKey var id: Int,
        var inProduction: Boolean,
        var languages: List<String>,
        var lastAirDate: String,
        var name: String,
        var networks: List<Network>,
        var numberOfEpisodes: Int,
        var numberOfSeasons: Int,
        var originCountry: List<String>,
        var originalLanguage: String,
        var originalName: String,
        var overview: String,
        var popularity: Double,
        var posterPath: String,
        var productionCompanies: List<ProductionCompany>,
        var seasons: List<Season>,
        var status: String,
        var type: String,
        var voteAverage: Double,
        var voteCount: Int,
        val credits: Credits
)

data class CreatedBy(var id: Int, var name: String, var gender: Int, var profilePath: String)

data class Network(var id: Int, var name: String)

data class Season(var airDate: String, var episodeCount: Int, var id: Int, var posterPath: String, var seasonNumber: Int)