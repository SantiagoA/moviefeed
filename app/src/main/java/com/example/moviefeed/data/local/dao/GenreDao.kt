package com.example.moviefeed.data.local.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.example.moviefeed.data.local.model.Genre

/**
 * Dao for [Genre] entity
 *
 * @author santiagoalvarez
 */
@Dao
abstract class GenreDao : BaseDao<Genre> {

    @Query("SELECT * FROM genre")
    abstract fun getGenres(): LiveData<List<Genre>>

    @Query("SELECT * FROM genre WHERE id = :id")
    abstract fun getGenre(id: Int): LiveData<Genre>

    @Query("SELECT * FROM genre WHERE id IN (:ids)")
    abstract fun getGenres(ids: List<Int>): LiveData<List<Genre>>

    @Query("DELETE FROM genre")
    abstract fun deleteAll()

    @Transaction
    open fun updateGenre(list: List<Genre>) {
        deleteAll()
        insert(*list.toTypedArray())
    }
}
