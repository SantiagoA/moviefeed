package com.example.moviefeed.view

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.util.SparseArray
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.GridLayoutAnimationController
import com.example.moviefeed.R


/**
 * Implementation of [RecyclerView] that responds to pinch gestures converting the list to a grid list
 *
 * FIXME save state not working
 * @author santiagoalvarez
 */
class GestureAwareRecyclerView : RecyclerView, ScaleGestureDetector.OnScaleGestureListener {

    val TAG = "GestureAwareRecyclerVie"

    private val NO_RES = 0
    private val DEFAULT_SPAN_COUNT = 2

    private lateinit var pinchGestureDetector: ScaleGestureDetector
    private lateinit var currentLayoutManagerType: LayoutManagerType

    private var linearLayoutAnimRes: Int = NO_RES
    private var gridLayoutAnimRes: Int = NO_RES
    private var spanCount: Int = DEFAULT_SPAN_COUNT

    enum class PinchGestureType { PINCH_OPEN, PINCH_CLOSED, NONE }

    enum class LayoutManagerType { LINEAR, GRID }

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        isSaveEnabled = true

        pinchGestureDetector = ScaleGestureDetector(context, this).apply { isQuickScaleEnabled = false }

        layoutManager.let {
            currentLayoutManagerType = if (it is GridLayoutManager) LayoutManagerType.GRID else LayoutManagerType.LINEAR
        }

        context.obtainStyledAttributes(attrs, R.styleable.GestureAwareRecyclerViewStyleable, defStyle, NO_RES)
                .apply {
                    linearLayoutAnimRes = getResourceId(R.styleable.GestureAwareRecyclerViewStyleable_layoutAnimation, NO_RES)
                    gridLayoutAnimRes = getResourceId(R.styleable.GestureAwareRecyclerViewStyleable_gridLayoutAnimation, NO_RES)
                    spanCount = getInt(R.styleable.GestureAwareRecyclerViewStyleable_spanCount, DEFAULT_SPAN_COUNT)
                }.recycle()
    }

    override fun onTouchEvent(e: MotionEvent?): Boolean {
        pinchGestureDetector.onTouchEvent(e)
        return super.onTouchEvent(e)
    }

    override fun attachLayoutAnimationParameters(child: View?, params: ViewGroup.LayoutParams?, i: Int, c: Int) {
        if (adapter != null && layoutManager is GridLayoutManager) {
            try {
                var animationParams: GridLayoutAnimationController.AnimationParameters? =
                        params?.layoutAnimationParameters as? GridLayoutAnimationController.AnimationParameters

                if (animationParams == null) {
                    animationParams = GridLayoutAnimationController.AnimationParameters()
                    params?.layoutAnimationParameters = animationParams
                }

                val columns = (layoutManager as GridLayoutManager).spanCount

                with(animationParams) {
                    count = c
                    index = i
                    columnsCount = columns
                    rowsCount = c / columns

                    val invertedIndex = c - 1 - i
                    column = columns - 1 - invertedIndex % columns
                    row = animationParams.rowsCount - 1 - invertedIndex / columns
                }
            } catch (ex: ClassCastException) {
                Log.d(TAG, "cast fails!: ")
            }
        } else {
            super.attachLayoutAnimationParameters(child, params, i, c)
        }
    }

    override fun onScale(detector: ScaleGestureDetector?) = false

    override fun onScaleBegin(detector: ScaleGestureDetector?) = true

    override fun onScaleEnd(detector: ScaleGestureDetector?) {
        val scaleFactor: Float = detector?.scaleFactor ?: -1F
        val pinchGestureType = when (scaleFactor) {
            in 1.5F..3F -> PinchGestureType.PINCH_OPEN
            in 0F..0.8F -> PinchGestureType.PINCH_CLOSED
            else -> PinchGestureType.NONE
        }
        onPinchEvent(pinchGestureType)
    }

    private fun onPinchEvent(pinchGestureType: PinchGestureType) {
        when (pinchGestureType) {
            GestureAwareRecyclerView.PinchGestureType.PINCH_OPEN -> {
                if (layoutManager is GridLayoutManager) {
                    LayoutManagerType.LINEAR.let {
                        setLayoutManager(it)
                        setLayoutAnimation()
                    }
                }
            }
            GestureAwareRecyclerView.PinchGestureType.PINCH_CLOSED -> {
                if (layoutManager !is GridLayoutManager) {
                    LayoutManagerType.GRID.let {
                        setLayoutManager(it)
                        setLayoutAnimation()
                    }
                }
            }
            GestureAwareRecyclerView.PinchGestureType.NONE -> {
                // no-op
            }
        }
    }

    private fun setLayoutManager(layoutManagerType: LayoutManagerType) {
        val manager: LayoutManager = when (layoutManagerType) {
            GestureAwareRecyclerView.LayoutManagerType.LINEAR -> LinearLayoutManager(context)
            GestureAwareRecyclerView.LayoutManagerType.GRID -> GridLayoutManager(context, spanCount)
        }
        currentLayoutManagerType = layoutManagerType
        layoutManager = manager
    }

    private fun setLayoutAnimation() {
        val res: Int = when (currentLayoutManagerType) {
            GestureAwareRecyclerView.LayoutManagerType.LINEAR -> linearLayoutAnimRes
            GestureAwareRecyclerView.LayoutManagerType.GRID -> gridLayoutAnimRes
        }
        layoutAnimation = AnimationUtils.loadLayoutAnimation(context, res)
    }

    //region save state
    override fun onSaveInstanceState(): Parcelable {
        val state = GestureAwareRecyclerViewSavedState(super.onSaveInstanceState())
        state.value = currentLayoutManagerType
        return state
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is GestureAwareRecyclerViewSavedState) {
            super.onRestoreInstanceState(state)
            return
        }

        currentLayoutManagerType = state.value
        super.onRestoreInstanceState(state.superState)
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>?) {
        dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>?) {
        dispatchFreezeSelfOnly(container)
    }

    private class GestureAwareRecyclerViewSavedState : BaseSavedState {

        internal lateinit var value: LayoutManagerType

        constructor(source: Parcelable) : super(source)

        constructor(source: Parcel?, loader: ClassLoader?) : super(source) {
            value = source?.readSerializable() as LayoutManagerType
        }

        override fun writeToParcel(dest: Parcel?, flags: Int) {
            super.writeToParcel(dest, flags)
            dest?.writeSerializable(value)
        }

        companion object {
            val CREATOR: Parcelable.ClassLoaderCreator<GestureAwareRecyclerViewSavedState> =
                    object : Parcelable.ClassLoaderCreator<GestureAwareRecyclerViewSavedState> {
                        override fun createFromParcel(source: Parcel?, loader: ClassLoader?): GestureAwareRecyclerViewSavedState {
                            return GestureAwareRecyclerViewSavedState(source, loader)
                        }

                        override fun createFromParcel(source: Parcel): GestureAwareRecyclerViewSavedState {
                            return GestureAwareRecyclerViewSavedState(source, null)
                        }

                        override fun newArray(size: Int): Array<GestureAwareRecyclerViewSavedState> {
                            return newArray(size)
                        }
                    }
        }

    }
    //endregion
}
