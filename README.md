# MovieFeed

Simple application that shows a categorised list of  Movie/TV shows and it's details.

# Desc
La arquitectura de la aplicación fue hecha de tal manera que se lograran dos cosas:
	1. Separación de intereses (SoC)
	2. UI guiada por modelo
Con esos principios como guía se aplican conceptos de S.O.L.I.D como Inversión de dependencias, responsabilidad única, substitución de Liskov y segregación de la interfaz, etc.
Se Logra entonces tener una arquitectura MVVM decente (no exhaustiva) desacoplada y cohesiva. Usando herramientas como: LiveData, Room, ViewModel, Databinding, Dagger2 y otras.

Capas: típicas de MVVM. 
	1. Vista, guiada por modelo se usa Databinding extensivamente, controladores encargados de manejar lógica de animaciones, de widgets, etc. 
	2. ViewModel, controlador encargado de recibir, procesar interacciones, datos, solicitar datos a el Manager
	3. Manager único punto de acceso a la capa de datos y persistencia, Se usa el concepto de "Single source of truth" de modo que los datos siempre provienen de la DB (Room),
		en caso de haber datos en la misma se hace el respectivo llamado a la los servicios REST y se almacena la respuesta en la DB para luego ser despachada a clientes. usa LiveData para exponer los datos.

Responsabilidades: (ver Kdoc en downloads)

# preguntas 
	1. Responsabilidad única: Cada componente, clase debe hacer una sola cosa y nada más.
	2. Código limpio. bajo acoplamiento logrado con modelos específicos para cada capa uso de mapeos para pasar de una capa a otra, modularización para separar conceptos, uso de namming útil y sugerente
		Alta cohesión muy relacionada con Única responsabilidad, cada clase debe tener alojar y procesar datos que sean relevantes y específicos del nivel en que se encuentra.
		Alta testeablidad, pruebas unitarias, pruebas de UI, pruebas de integración. 
#TODO
Testing: agregar pruebas
Definir claramente el algoritmo (transaction) para almacenar los datos del servicio (actualmente solo se reemplaza)
Mejorar la experiencía de usuario (dialogs, mensajes, etc)
Buscar memory leaks

#FIXME
Animaciones: fallos asocidos a "race conditions" sincronización, etc.
Viewpager: fallos en algunas transiciones
Gesture RecyclerView: persistir el estado a travez de cambios en la configuración.

